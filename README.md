# Smarty2 Lint

Checks smarty template syntax (for [Smarty2](https://github.com/smarty-php/smarty/tree/v2.6.31/)). If there are syntax errors detected, a ```Smarty2Lint\Exception``` will be thrown.

## How to use ?

Create a new instance of ```Smarty2Lint\Examine``` and call the ```Smarty2Lint\Examine::syntax()``` method.

```php
$l = new Smarty2Lint\Examine;
$l->syntax('proba.tpl');
```

If there is a syntax error, there will be a ```Smarty2Lint\Exception``` thrown:

```bash
Fatal error: Uncaught exception 'Smarty2Lint\Exception' with message '[in proba.tpl line 37]: syntax error: unrecognized tag: $$error (Smarty_Compiler.class.php, line 441)' in smarty_lint/src/Compiler.php:26
```

Use try/catch to capture the exception thrown:

```php
$l = new Smarty2Lint\Examine;
try {
$l->syntax('proba.tpl');
}
catch (Smarty2Lint\Exception $e)
{
	echo $e->getMessage();
}
```

## Customized Smarty Object

Using the Smarty object right from the library is very often pointless. This is because your project will almost always have that Smarty object modified with a lot changes, such as:

* where to write compiled templates at ```Smarty::$compile_dir```
* where to look for new tags ```Smarty::$plugins_dir```
* other settings and configurations
* registered functions, blocks, modifiers

You can use your own Smarty object for the syntax checks:

```php
// customized Smarty object, with an extra modifier registered
$s = new \Smarty;
$s->register_modifier('strip', 'trim');

// inject the new Smarty object
$l = new Smarty2Lint\Examine;
$l->setSmarty($s);

$l->syntax('templates/index.tpl');
```

## Unrecognized Tags

Majority of reported syntax errors are for missing tags, e.g. smarty functions, smarty blocks and smarty modifiers that are not registered.

There is a configuration setting to make ```Smarty2Lint\Examine``` ignore the unrecognized tags, and collect a list of of where they were detected:

```php
$l = new Smarty2Lint\Examine;
$l->ignoreUnrecognizedTag = true;
$l->syntax('templates/index.tpl');

print_r($l->unrecognizedTags);
```

To see the collected unrecognized tags, look at ```Smarty2Lint\Examine::$unrecognizedTags```. It is an array with all the unrecognized tags as the keys, and the values are arrays of template references where they were detected. Format is "resource_name:line_of_code", so "proba.tpl:27" means found inside "proba.tpl" template at line 27. Here's what it looks like when dumped with ```print_r($l->unrecognizedTags);```:

```
Array
(
    [breadcrumb] => Array
        (
            [0] => proba.tpl:27
        )

    [/breadcrumb] => Array
        (
            [0] => proba.tpl:29
        )

    [errors] => Array
        (
            [0] => proba.tpl:129
        )

)
```
