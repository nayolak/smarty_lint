<?php /**
* @package Smarty2Lint
*/

namespace Smarty2Lint;

/**
* Examines template files for syntax errors
*/
class Examine
{
	/**
	* @var Smarty
	*/
	protected $smarty;

	/**
	* Set your own Smarty instance
	*
	* Use this method to inject your own Smarty object, with all the
	* settings and configuration applied to it, such as plugins_dir,
	* extra resources, etc.
	*
	* @param Smarty  $smarty
	*/
	public function setSmarty(\Smarty $smarty)
	{
		$this->smarty = $smarty;
	}

	/**
	* Fetch the Smarty instance that is going to be used for the syntax check
	* @return Smarty
	*/
	public function getSmarty()
	{
		if (empty($this->smarty))
		{
			$this->smarty = new \Smarty;
		}
		return $this->smarty;
	}

	/**
	* @var boolean whether to ignore unrecognized tags
	*/
	public $ignoreUnrecognizedTag = false;

	/**
	* @var array list of all detected unrecognized tags
	*/
	public $unrecognizedTags = array();

	/**
	* Performs syntax check
	* @param string $resource_name
	*/
	public function syntax($resource_name)
	{
		$this->getSmarty();
		$this->smarty->compiler_class = '\\Smarty2Lint\\Compiler';

		if (!is_dir($this->smarty->compile_dir))
		{
			$this->smarty->compile_dir = '/tmp';
		}

		if (!$this->smarty->template_exists($resource_name))
		{
			throw new Exception(
				"Unable to read resource: '{$resource_name}'"
			);
		}

		try {
			$compile_path = $this->smarty->_get_compile_path($resource_name);
			$this->smarty->_compile_resource($resource_name, $compile_path);
		}
		catch (Exception $e)
		{
			// register missing tag and try again
			//
			if ($this->ignoreUnrecognizedTag)
			{
				if ($tag = $this->unrecognizedTag($e, $resource_name))
				{
					return $this->syntax($resource_name);
				}
			}
		}

		// clean up registered missing tags
		//
		if ($this->ignoreUnrecognizedTag)
		{
			foreach ($this->unrecognizedTags as $tag => $i)
			{
				$this->smarty->unregister_function($tag);
			}
		}

		if (isset($e))
		{
			throw $e;
		}
	}

	/**
	* Extracts the unrecognized tag from the exception
	*
	* @param Smarty2Lint\Exception $e
	* @param string $resource_name
	* @return string
	*/
	protected function unrecognizedTag(Exception $e, $resource_name)
	{
		if (!preg_match(
			'~\[in (.+) line (\d+)\]\:.*unrecognized tag \'(.+)\'~',
			$e->getMessage(),
			$R))
		{
			return '';
		}

		$tag = $R[3];

		// keep a tally of unrecognized tags
		//
		if (empty($this->unrecognizedTags[$tag])
			|| !in_array($resource_name,
				$this->unrecognizedTags[$tag]))
		{
			$this->unrecognizedTags[$tag][] = $R[1] . ':' . $R[2];
		}

		// use "trim" as dummy tag
		//
		$this->smarty->register_function($tag, 'trim');

		return $tag;
	}

}
