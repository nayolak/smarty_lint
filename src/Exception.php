<?php /**
* @package Smarty2Lint
*/

namespace Smarty2Lint;

/**
* Exceptions thrown on syntax errors
*/
class Exception extends \Exception
{
	
}
