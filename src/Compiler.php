<?php /**
* @package Smarty2Lint
*/

namespace Smarty2Lint;

/**
* Replaces original compiler in order to override {@link \Smarty_Compiler::_syntax_error()}
*/
class Compiler extends \Smarty_Compiler
{
	/**
	* Instead of calling {@link trigger_error()} with E_USER_ERROR this will
	* throw {@link Smarty2Lint\Exception}
	*
	* @param string $error_msg
	* @param integer $error_type
	*/
        function trigger_error($error_msg, $error_type = E_USER_WARNING)
	{
		if (E_USER_ERROR != $error_type)
		{
			return parent::trigger_error($error_msg, $error_type);
		}

		throw new Exception($error_msg);
	}
}
